# Journal v3: Fragments  

Chaque jour, tu as devant toi une page blanche.

C'est ton brouillon du jour, ton journal. Tu écris ce que tu veux toutes tes notes, pensées, liste de trucs à faire ... Sans besoin d'aucune syntaxe initiale. 

Chaque jour une nouvelle page blanche.

Tu écris. et puis tu cliques sur le bouton "Fragment", pour déclarer que tu écris un nouveau fragment:  
**un séparateur s'affiche à l'écran**  et tu écris à nouveau, librement.

- Tu peux ajouter des tags à tes fragments.
<!--.element: class="fragment fade-up" data-fragment-index="1"-->   
- Tu peux afficher et travailler sur des compilations de fragments (notes)
<!--.element: class="fragment fade-up" data-fragment-index="2"-->  
- Tu peux exporter le tout dans un simple fichier texte lisible.
<!--.element: class="fragment fade-up" data-fragment-index="3"-->  
- Tu peux collaborer avec fragment
<!--.element: class="fragment fade-up" data-fragment-index="4"-->  
- Tu peux parler à Fragments
<!--.element: class="fragment fade-up" data-fragment-index="5"-->  



***



## Parsing  Fragments

Le séparateur principal est simple:

`---------------------`

Les tags suivent le séparateur, une paire `key:value`par ligne.
Un saut de ligne sépare le dernier tag du début de la note (nécessaire?)

On utilise markdown pour la syntaxe du texte

## Basic keys

```
-----------------------------
author:makayabou
date:19/05/2019
time_start:14:00AM
time_end:00:00PM
time_active:90M

Mon brouillon du jour, dans lequel je n'ai **pas écrit grand chose**..

-------------------------
```



***



## Tags
```
tag:projet1
tag:projet2/serviceA
tag:compte-rendu
tag:reunion
tag:ingerence

Et puis là j'écris un truc des notes, n'importe quoi, jusqu'à ce que je pense à autre chose.. Là un nouveau séparateur vient cloturer le fragment et créer un nouveau fragment (en initialisant les tags)

-----------------------------------------
```

**la dénomination arborescente des labels (projet2/serviceA).**

Du point de vue de l'utilisateur ça permet d'avoir une pseudo arborescence, mais en même temps de pouvoir obtenir facilement des recherches croisés

  **Par exemple:**  
    Je démarre mon fragment, je clique sur le tag projetA, et s'offrent à moi les tags associés:  
       projetA/plan-de-financement, projetA/RH, etc.



***



## Modifications
Les notes sont des compilations de fragments qui partagent des tags en commun.
Je recherche tous les fragments qui contiennent le tag: "bête d'idée de nom de projet" . J'obtiens une liste de fragments que je peux consulter individuellement.
Ou je peux accéder à la note correspondante.
La note est donc en fait une compilation de fragments, versionnée, sur laquelle travailler. 
Qunad on modifie un fragment existant, on va créer un nouveau fragment, avec le nouveau timestamp, qui fait référence au fragment qu'il modifie.
On aura ainsi un historique des notes et quand on appelle la note elle apparait sous sa dernière version:

Ainsi la note "discours" de l'exemple suivant apparaît ainsi:

```txt
un premier blabla modifié
un deuxième blabla modifié car il était déjà top mais là...
```

***


Au 17/5:   <!--.element: class="fragment fade-left" data-fragment-index="1"--> 
```
id:#18278   -- generated
tag:discours
tag:cibleA

un premier blabla

-----------------------
date:17/05/2019
id:#18279
name:la meilleure prose de ma carrière
tag:discours
tag:cibleB

un deuxième blabla

-----------------------
``` 

<!--.element: class="fragment fade-left" data-fragment-index="1"-->  
Au 18/5: <!--.element: class="fragment fade-left" data-fragment-index="2"-->  


```
date:18/05/2019
id=#18281   -- generated
tag:discours
tag:cibleB
modifies:#18278

un premier blabla modifié
------------------------
date:18/05/2019
id=#18282
tag:discours
tag:cibleB
modifies:17/05/2019-la meilleure prose de ma carrière

un deuxième blabla modifié car il était déjà top mais là...
------------------------
```  
<!--.element: class="fragment fade-left" data-fragment-index="2"-->  


***


## Collaborer par Fragments
Les tags suivants permettent le travail collaboratif sur les notes:
```
fork:#id      --is forked from #id
merge:#id  -- has accepted fork #id as modification
access:private    --joker can be easily un/-checked
access:equipe/ro
access:ca/rw
```
## Joindre des éléments <!--.element: class="fragment fade-up"  data-fragment-index="1"-->  
```
doc:nc.gn.fr/a.pdf
link:si.te
link:sit.e2
fragment:#id
```  
<!--.element: class="fragment fade-up"  data-fragment-index="1"-->   
## integration   <!--.element: class="fragment fade-up"  data-fragment-index="2"-->  
```
origin:mattermost          -- chaque message envoyé dans mattermost constitue un nouveau fragment
origin: caldav           --chaque événement créé dans l'agenda > fragment
todo:en parler à Djamal  --dans l'autre sens, la clé `todo` provoque la création d'une tâche 
					-- d'une tâche dans l'appli calldav
```
<!--.element: class="fragment fade-up"  data-fragment-index="2"-->


***


## Parler par Fragments
L'idée vient de l'usage des mémos vocaux sur Snapchat ou imessage. Ils séduisent parcequ'ils sont rapides à produire et écouter, mais ne sont pas intrusifs puisqu'on les écoute quand on veut.  

L'idée vient aussi des recherches que j'ai eu à faire à la Fac pour les retranscriptions d'entretiens.  
Il y a des logiciels qui font la retranscription automatique d'un entretien, te proposent une interface pour corriger le texte et finalisent ton export avec un fichier html qui contient le texte l'audio et la synchronisation par clic.  
Enfin le concept ça peut aussi toucher aux commandes vocales. POur de la prise de notes c'est intéressant c'est commes les enquéteurs du FBI avec leur dictaphone.  

Les moteurs open source autour de la reconnaisance vocale c'est CMU Sphinx, Kaldi, Mozilla Deep Speech.
Donc, on enregistre un mémo, il est enregistré comme un fragment, avec l'audio lié, ce qui permet d'en découvrir le contenu en lisant ou en écoutant l'original.  
```    
origin: Mattermost/VoiceMemo
voice:messagefile.ht
```



***



## Des tags auto-complétifs
On a vu déjà que les tags associés à un tag apparaissent au clic sur un tag.    
Mais il faut que ça aille plus loin. Il faut que les fragments tentent de s'auto-marquer, ou au moins de suggérer des solutions pour chaque famille de tag:  
- type de tâche (administration, comptabilité, type/animation, type/codage..)
- moment (idéee,préparation, notes de bord, moment/compte-rendu)
- projet(atelier 20e chaise, formation CNAM, accueil structure)
- ...  
en fonction de ce qui s'écrit dans le fragment et des tags déjà sélectionnées, en fonction des dossiers crées dans le dossier du projet concerné.. (des habitudes de l'utilisateur...)  

Les objectifs:
- améliorer la réflexivité
- réduire le travail d'indexation
- faciliter la recherche d'informations
- favoriser la collaboration


***


## Ascii Doctor  
Peut-être qu'il faudrait utiliser [Ascii Doc](https://asciidoctor.org/) 

Clique sur l'image suivante pour voir un exemple de génération de documentation automatique basée sur Ascii Doctor  
[![Continuous Documentation with Jenkins Gradle Asciidoctor](https://img.youtube.com/vi/O2wToEdPmSc/0.jpg)](https://www.youtube.com/watch?v=O2wToEdPmSc "Continuous Documentation with Jenkins Gradle Asciidoctor")
